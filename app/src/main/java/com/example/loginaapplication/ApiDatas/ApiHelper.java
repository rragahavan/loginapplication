package com.example.loginaapplication.ApiDatas;

import android.content.Context;
import com.example.loginaapplication.Model.UserModel;
import com.example.loginaapplication.R;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class ApiHelper {
    final private Context context;
    public ApiHelper(Context context){
        this.context = context;
    }

    public String sendUserDetails(UserModel userModel)
    {
        URL url;
        StringBuilder sb =new StringBuilder();
        try {

            String query = "user_name="+URLEncoder.encode(userModel.getEmail(),"UTF-8");
            query += "&";
            query += "password="+URLEncoder.encode(userModel.getPassword(),"UTF-8") ;
            url=new URL(context.getString(R.string.base_url)+ApiUrl.LOGIN_URL);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.addRequestProperty("X-CLIENT","uK7tAfi3JwCXMikBkRU60x1WYYiKg1td+rPGjS7LSjI=");
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-length", String.valueOf(query.length()));
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setRequestProperty("X-Requested-With", "XMLHttpRequest");
            urlConnection.connect();
            OutputStream os = new BufferedOutputStream(urlConnection.getOutputStream());
            os.write(query.getBytes());
            os.flush();
            os.close();
            int status = urlConnection.getResponseCode();
            if(status==201||status==200){
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
            }

            else
            {

                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getErrorStream()));
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();

            }
            urlConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
