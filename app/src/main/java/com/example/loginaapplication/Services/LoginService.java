package com.example.loginaapplication.Services;

import android.content.Context;
import com.example.loginaapplication.ApiDatas.ApiHelper;
import com.example.loginaapplication.Model.UserModel;
import com.example.loginaapplication.Network.ConnectionDetector;
import com.example.loginaapplication.R;
import com.example.loginaapplication.Util.SaveUserLogin;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginService {

    private Context context;
    SaveUserLogin saveUserLogin;
    ApiHelper apiHelper;
    ConnectionDetector connectionDetector;

    public LoginService(Context context){
        this.context = context;
    }

    public boolean sendUserData(UserModel userModel)
    {
        apiHelper=new ApiHelper(context);
        boolean isValid=false;
        saveUserLogin=new SaveUserLogin();
        connectionDetector=new ConnectionDetector(context);
        validate(userModel);
        String message;
        if(!(userModel.getHasErrors()))
        {
            JSONObject User = new JSONObject();
            try {
                 if(connectionDetector.isNetworkAvailable()) {
                    message = apiHelper.sendUserDetails(userModel);
                    User = new JSONObject(message);
                    if (User.getString("success").equals("true")) {
                        SaveUserLogin.setUserId(context, User.getLong("clientId"));
                        isValid = true;

                    } else {
                        userModel.setErrors(R.string.invalid_password_email);
                    }
                }
                else
                {
                    userModel.setErrors(R.string.network_failure);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isValid;
    }

    public void validate(UserModel userModel) {
        userModel.Errors.clear();
        if (userModel.getEmail().equals(""))
            userModel.setErrors(R.string.enter_email);
        if (userModel.getPassword().equals(""))
            userModel.setErrors(R.string.enter_password);

    }
}
