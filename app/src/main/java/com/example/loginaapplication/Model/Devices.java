package com.example.loginaapplication.Model;


public class Devices {
    private String SerialNumber;

    private String Id;

    public String getSerialNumber ()
    {
        return SerialNumber;
    }

    public void setSerialNumber (String SerialNumber)
    {
        this.SerialNumber = SerialNumber;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [SerialNumber = "+SerialNumber+", Id = "+Id+"]";
    }
}
