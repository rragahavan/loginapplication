package com.example.loginaapplication.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class SaveUserLogin {

    static final String PREF_USER_ID= "userId";

    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }
    public static void setUserId(Context ctx, Long userId)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putLong(PREF_USER_ID, userId);
        editor.commit();
    }

    public static Long getUserId(Context ctx)
    {
        return getSharedPreferences(ctx).getLong(PREF_USER_ID, 0);
    }
}
