package com.example.loginaapplication.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.example.loginaapplication.R;
import com.example.loginaapplication.Util.SaveUserLogin;

public class DemoActivity extends AppCompatActivity {

    Button logoutButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);

        logoutButton=(Button)findViewById(R.id.logoutButton);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SaveUserLogin.setUserId(DemoActivity.this,0L);
                Intent gotoLoginPage = new Intent(DemoActivity.this, LoginPageActivity.class);
                startActivity(gotoLoginPage);
            }
        });
    }
}
