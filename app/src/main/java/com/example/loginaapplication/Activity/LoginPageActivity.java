package com.example.loginaapplication.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.loginaapplication.Model.UserModel;
import com.example.loginaapplication.R;
import com.example.loginaapplication.Services.LoginService;
import com.example.loginaapplication.Util.SaveUserLogin;

public class LoginPageActivity extends AppCompatActivity {
    EditText userPassword,
            userEmail;
    private String email,password;
    Button loginButton;
    private UserModel userModel;
    private LoginService loginService;
    private boolean isValid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        if(SaveUserLogin.getUserId(getApplicationContext())!=0)

        {
            Intent gotoNextPage = new Intent(LoginPageActivity.this, DemoActivity.class);
            startActivity(gotoNextPage);
        }



        userEmail=(EditText)findViewById(R.id.emailAddressText);
        userPassword=(EditText)findViewById(R.id.passwordText);
        userModel=new UserModel();
        loginService=new LoginService(LoginPageActivity.this);
        loginButton=(Button)findViewById(R.id.loginButton);

            loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email=userEmail.getText().toString().trim();
                password=userPassword.getText().toString().trim();
                userModel.setEmail(email);
                userModel.setPassword(password);
                isValid=loginService.sendUserData(userModel);
                if(isValid)
                {

                    Toast.makeText(LoginPageActivity.this,R.string.login_success,Toast.LENGTH_LONG).show();
                    Intent gotoNextPage = new Intent(LoginPageActivity.this, DemoActivity.class);
                    startActivity(gotoNextPage);
                }
                else
                {
                    if(userModel.getErrors().size()!=0) {
                        Toast.makeText(LoginPageActivity.this, userModel.getErrors().get(0), Toast.LENGTH_LONG).show();
                    }

                }
            }
        });


        LinearLayout touchInterceptor = (LinearLayout) findViewById(R.id.linearMain);
        touchInterceptor.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    if (userEmail.isFocused() || userPassword.isFocusable()) {
                        Rect outRect = new Rect();
                        userEmail.getGlobalVisibleRect(outRect);
                        userPassword.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                            userEmail.clearFocus();
                            userPassword.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                return false;
            }
        });


    }
}
